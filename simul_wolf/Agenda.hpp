enum Impl_Type {heap, queue};


class Agenda
{
public:
    char * AgendaName;
    int ImplType;
    int InitSize;
    int ExpandSize;
    int CurrentSize;
    Event ** storage; // for array of pointers to to Event objects

    Agenda(){};
    Agenda(Impl_Type it, int isize, int esize)
    {
        ImplType = it;
        InitSize = isize;
        ExpandSize = esize;
        storage = new Event*[InitSize];
        CurrentSize = isize;
    };
    void setInitSize();
    int getInitSize();
    int addEvent(Event * e)
    {
        return 0;//0 or 1 for failure or success
    };
    int remEvent(Event * e)
    {
        return 0;//0 or 1 for failure or success
    };
    int remCurrentEvent()
    {
        return 0;//0 or 1 for failure or success
    };
    void findEvent();
    void expand()
    {
        Event ** x;
        int sz;
        sz = CurrentSize + ExpandSize;
        x = new Event*[sz];
        for (int i = 0; i < CurrentSize; i++){
            x[i] = storage[i];
        }
        delete [] storage;
        storage = x;
        CurrentSize = CurrentSize + ExpandSize;
        // add method to free original storage
    };
    void contract();
    void print();

};
