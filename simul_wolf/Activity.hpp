#include <string.h>
#include <iostream>

using namespace std;

class Activity
{
public:
    char * ActName;
    char * ActDesc;
    int InitDuration;
    int RemainingDuration;
    Activity(){};
    Activity(char * nm)
    {
        ActName = nm;
        InitDuration = 0;
        RemainingDuration = 0;
    };
    Activity(char * nm, int initdur)
    {
        ActName = nm;
        InitDuration = initdur;
        RemainingDuration = initdur;
    };
    void setActName();
    char * getActName();
    void setInitDuration(int n)
    {
        InitDuration = n;
    };
    void setRemainingDuration(int n)
    {
        RemainingDuration = n;
    };
    int getInitDuration()
    {
        return InitDuration;
    };
    int getRemainingDuration()
    {
        return RemainingDuration;
    };
    void setActDesc(char * d)
    {
        ActDesc = d;
    };
    char * getActDesc()
    {
        return ActDesc;
    };
    void print()
    {
        cout << "Name: " << ActName << endl;
        cout << "Description: " << ActDesc << endl;
        cout << "Initial Duration: " << InitDuration << endl;
        cout << "Remaining Duration: " << RemainingDuration << endl;
    };
};
